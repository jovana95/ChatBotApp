﻿using ChatBot.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChatBot.Controllers
{
    public class UserController : ApiController
    {
        private ChatbotDBEntities db = new ChatbotDBEntities();
    
        [Route("api/getUsers")]
        public DbSet<User> Get()
        {
            return db.Users;
        }

        [Route("api/user/{id}")]
        public User Get(int id)
        {
            return db.Users.Where(x => x.id_user == id).FirstOrDefault();
        }

        // POST: api/User
        public void Post([FromBody]User user)
        {
            Console.WriteLine(user.Email+"aaaaaaaaaaaaaaaaaaaaaaaaaaa");
            db.Users.Add(user);
            db.SaveChanges();
        }

        // PUT: api/User/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/User/5
        public void Delete(int id)
        {
        }
    }
}
