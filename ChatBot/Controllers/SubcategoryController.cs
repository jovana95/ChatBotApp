﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChatBot.Controllers
{
    public class SubcategoryController : ApiController
    {
        // GET: api/Subcategory
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Subcategory/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Subcategory
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Subcategory/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Subcategory/5
        public void Delete(int id)
        {
        }
    }
}
