﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace ChatBot.Dialogs
{
    [Serializable]
    public class RootDialog : IDialog<object>
    {
        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            /*     var activity = await result as Activity;

                  // Calculate something for us to return
                  int length = (activity.Text ?? string.Empty).Length;

                  // Return our reply to the user
                  await context.PostAsync($"You sent {activity.Text} which was {length} characters");

                  context.Wait(MessageReceivedAsync);
            */
            IEnumerable<string> yesNo = new List<string>
            {
                "Yes",
                "No",            
            };

            PromptDialog.Choice(context, AfterPromptDialogChoice, yesNo, "Do you want to buy a car?");

        }

        private async Task AfterPromptDialogChoice(IDialogContext context, IAwaitable<string> result)
        {
            string input = await result;
            if(input.Equals("Yes"))
               await context.PostAsync("Yes");
            else
                await context.PostAsync("No");

            context.Wait(this.MessageReceivedAsync);
        }
    }
}