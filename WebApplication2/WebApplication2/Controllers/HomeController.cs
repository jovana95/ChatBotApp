﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult MyProfile()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(Register r, string ReturnUrl = "")
        {
            using (ChatbotDBEntities db = new ChatbotDBEntities())
            {
                var user = db.Users.Where(a => a.Username.Equals(r.Username) || a.Password.Equals(r.Password)).FirstOrDefault();
                if (user == null)
                {
                    if (Url.IsLocalUrl(ReturnUrl))
                    {
                        return Redirect(ReturnUrl);
                    }
                    else
                    {
                        db.Users.Add(new WebApplication2.User { Username = r.Username, Password = r.Password, Email = r.Email, Adress = r.Address, FirstName = r.Firstname, LastName = r.Lastname });
                        db.SaveChanges();
                        return RedirectToAction("MyProfile", "Home");
                    }
                }
                else
                {
                    Response.Write("Username or password already exists in database.");
                }
            }
            ModelState.Remove("Password");
            return View();
        }

        [Authorize(Roles="Admin")]
        public ActionResult AdminIndex()
        {
            using(ChatbotDBEntities db = new ChatbotDBEntities())
            {
                AdminViewModel myModel = new AdminViewModel();
                List<Characteristic> c = (from a in db.Characteristics select a).ToList();
                myModel.characteristic = c;
                List<ClassOfCategory> c1 = (from a in db.ClassOfCategories select a).ToList();
                myModel.classes = c1;
                List<Model_vehicle> mv = (from a in db.Model_vehicle select a).ToList();
                myModel.models = mv;
                List<Manufacturer> m = (from a in db.Manufacturers select a).ToList();
                myModel.manufacturer = m;
                List<Package> p = (from a in db.Packages select a).ToList();
                myModel.package = p;
                List<Vehicle> v = (from a in db.Vehicles select a).ToList();
                myModel.vehicle = v;
                List<Category> c2 = (from a in db.Categories select a).ToList();
                myModel.category = c2;
                return View(myModel);
            }
        }


        public ActionResult EditCategory(int id)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();
            
                Category c = db.Categories.FirstOrDefault(x => x.id_category==id);
                return View(c);
     
            
        }

        [HttpPost]
        public void EditCategory(Category c)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();
            Category c1 = db.Categories.FirstOrDefault(x => x.id_category == c.id_category);
            c1.Name = c.Name;
            db.Entry(c1).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }


        public ActionResult EditModel(int id)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

             Model_vehicle c = db.Model_vehicle.FirstOrDefault(x => x.id_model == id);
            return View(c);
        }

        [HttpPost]
        public void EditModel(Model_vehicle c)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();
            Model_vehicle c1 = db.Model_vehicle.FirstOrDefault(x => x.id_model == c.id_model);
            c1.Name_model = c.Name_model;
            db.Entry(c1).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public ActionResult EditClasses(int id)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

            ClassOfCategory c = db.ClassOfCategories.FirstOrDefault(x => x.id_category == id);
            return View(c);
        }

        [HttpPost]
        public void EditClasses(ClassOfCategory c)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();
            ClassOfCategory c1 = db.ClassOfCategories.FirstOrDefault(x => x.id_class == c.id_class);
            c1.Name_subclass = c.Name_subclass;         
            db.Entry(c1).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public ActionResult EditManufacturer(int id)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

            Manufacturer c = db.Manufacturers.FirstOrDefault(x => x.id_manufacturer == id);
            return View(c);
        }

        [HttpPost]
        public void EditManufacturer(Manufacturer c)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();
            Manufacturer c1 = db.Manufacturers.FirstOrDefault(x => x.id_manufacturer == c.id_manufacturer);
            c1.Name_manufacturer = c.Name_manufacturer;
            c1.year_of_production = c.year_of_production;
            db.Entry(c1).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }


        public ActionResult EditPackage(int id)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

            Package c = db.Packages.FirstOrDefault(x => x.id_package == id);
            return View(c);
        }

        [HttpPost]
        public void EditPackage(Package c)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();
            Package c1 = db.Packages.FirstOrDefault(x => x.id_package == c.id_package);
            c1.Price = c.Price;
            c1.seats = c.seats;
            c1.wheels = c.wheels;
            c1.air_conditioning = c.air_conditioning;
            db.Entry(c1).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }


        public ActionResult EditCharacteristic(int id)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

            Characteristic c = db.Characteristics.FirstOrDefault(x => x.id_characteristic == id);
            return View(c);
        }

        [HttpPost]
        public void EditCharacteristic(Characteristic c)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();
            Characteristic c1 = db.Characteristics.FirstOrDefault(x => x.id_characteristic == c.id_characteristic);
            c1.dors = c.dors;
            c1.quantity = c.quantity;
            c1.color = c.color;
            db.Entry(c1).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }


        public void DeleteCategory(int id)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

            Category c = db.Categories.FirstOrDefault(x => x.id_category == id);
            db.Categories.Remove(c);
            db.SaveChanges();

        }

        public void DeleteClass(int id)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

            ClassOfCategory c = db.ClassOfCategories.FirstOrDefault(x => x.id_class == id);
            db.ClassOfCategories.Remove(c);
            db.SaveChanges();

        }

        public void DeleteModel(int id)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

            Model_vehicle c = db.Model_vehicle.FirstOrDefault(x => x.id_model == id);
            db.Model_vehicle.Remove(c);
            db.SaveChanges();

        }

        public void DeleteManufacturer(int id)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

            Manufacturer c = db.Manufacturers.FirstOrDefault(x => x.id_manufacturer == id);
            db.Manufacturers.Remove(c);
            db.SaveChanges();

        }

        public void DeletePackage(int id)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

            Package c = db.Packages.FirstOrDefault(x => x.id_package == id);
            db.Packages.Remove(c);
            db.SaveChanges();

        }

        public void DeleteCharacteristic(int id)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

            Characteristic c = db.Characteristics.FirstOrDefault(x => x.id_characteristic == id);
            db.Characteristics.Remove(c);
            db.SaveChanges();

        }

        public ActionResult AddCategory()
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

            Category c = new Category();
            return View(c);


        }

        public void AddCategory1(Category c)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();
            db.Categories.Add(c);
            db.SaveChanges();

        }

        public ActionResult AddClass(int id)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

            ClassOfCategory c = new ClassOfCategory();
            c.id_category = id;
            return View(c);


        }

        public void AddClass1(ClassOfCategory c)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();
            db.ClassOfCategories.Add(c);
            db.SaveChanges();

        }

        public ActionResult AddModel(int id)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

            Model_vehicle c = new Model_vehicle();
            c.id_manufacturer = id;
            return View(c);


        }

        public void AddModel1(Model_vehicle c)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();
            db.Model_vehicle.Add(c);
            db.SaveChanges();

        }
        public ActionResult AddManufacturer()
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

            Manufacturer c = new Manufacturer();
            return View(c);


        }

        public void AddManufacturer1(Manufacturer c)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();
            db.Manufacturers.Add(c);
            db.SaveChanges();

        }

        public ActionResult AddPackage()
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

            Package c = new Package();
            return View(c);


        }


        public void AddPackage1(Package c)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();
            db.Packages.Add(c);
            db.SaveChanges();

        }

        public ActionResult AddCharacteristic()
        {
            ChatbotDBEntities db = new ChatbotDBEntities();

            Characteristic c = new Characteristic();
            return View(c);


        }

        public void AddCharacteristic1(Characteristic c)
        {
            ChatbotDBEntities db = new ChatbotDBEntities();
            db.Characteristics.Add(c);
            db.SaveChanges();

        }

        public ActionResult Chat()
        {
            return View();
        }

    }
}