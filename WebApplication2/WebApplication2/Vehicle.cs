//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication2
{
    using System;
    using System.Collections.Generic;
    
    public partial class Vehicle
    {
        public int id_vehicle { get; set; }
        public Nullable<int> id_class { get; set; }
        public Nullable<int> id_model { get; set; }
    
        public virtual ClassOfCategory ClassOfCategory { get; set; }
        public virtual Model_vehicle Model_vehicle { get; set; }
    }
}
