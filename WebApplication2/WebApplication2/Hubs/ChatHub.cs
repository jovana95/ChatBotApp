﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using WebApplication2.Models;

namespace WebApplication2.Hubs
{
    public class ChatHub : Hub
    {
        public static ConcurrentDictionary<string, MyUserType> MyUsers = new ConcurrentDictionary<string, MyUserType>();
        private object data;

        public override Task OnConnected()
        {
            MyUsers.TryAdd(Context.ConnectionId, new MyUserType() { ConnectionId = Context.ConnectionId });
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            MyUserType garbage;

            MyUsers.TryRemove(Context.ConnectionId, out garbage);

            return base.OnDisconnected(stopCalled);
        }


        public void Send(string name, string message, string connId)
        {
            string newMessage="";
            newMessage += "Your answer is:" + message + "<br>";
            // Call the addNewMessageToPage method to update clients.
            if (message.Equals("Yes"))
            {
                ChatbotDBEntities db = new ChatbotDBEntities();
                List<Category> categories = new List<Category>();
                categories = (from c in db.Categories
                              select c).ToList();              
                newMessage += "Which type of vehicle category would you like?<br>";
                foreach (var category in categories)                
                    newMessage += "<a href='#' class='btn btn-green category' onclick='ispis2(this.innerHTML)' value='" + category.Name+"'>" +category.Name+"</a>";
                Clients.Client(connId).addNewMessageToPage(name, newMessage);
            }
            else
            {
                Clients.Client(connId).addNewMessageToPage(name, newMessage);

            }
        }


        public void Send2(string name, string message, string connId)
        {
            string newMessage = "";
            newMessage += "Your answer is:" + message + "<br>";
                ChatbotDBEntities db = new ChatbotDBEntities();
                List<ClassOfCategory> classes = new List<ClassOfCategory>();
            classes = (from c in db.ClassOfCategories
                          join c1 in db.Categories on c.id_category equals c1.id_category
                              where c1.Name==message
                              select c).ToList();
                newMessage += "Which type of vehicle class would you like?<br>";
                foreach (var class1 in classes)
                    newMessage += "<a href='#' class='btn btn-green category' onclick='ispis3(this.innerHTML)' value='" + class1.Name_subclass + "'>" + class1.Name_subclass + "</a>";
                Clients.Client(connId).addNewMessageToPage(name, newMessage);        
        }

        public void Send3(string name, string message, string connId)
        {
            string newMessage = "";
            newMessage += "Your answer is:" + message + "<br>";
            ChatbotDBEntities db = new ChatbotDBEntities();
            List<Manufacturer> manufacturers = new List<Manufacturer>();
            manufacturers = (from c in db.Model_vehicle
                       join c1 in db.Vehicles on c.id_model equals c1.id_model
                       join c2 in db.Manufacturers on c.id_manufacturer equals c2.id_manufacturer
                       join c3 in db.ClassOfCategories on c1.id_class equals c3.id_class
                       where c3.Name_subclass == message
                       select c2).ToList();
            newMessage += "Which type of vehicle class would you like?<br>";
            foreach (var manufacturer in manufacturers)
                newMessage += "<a href='#' class='btn btn-green category' onclick='ispis3(this.innerHTML)' value='" + manufacturer.Name_manufacturer + "'>" + manufacturer.Name_manufacturer + "</a>";
            Clients.Client(connId).addNewMessageToPage(name, newMessage);
        }

    }
}