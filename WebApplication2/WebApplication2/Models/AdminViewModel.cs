﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class AdminViewModel
    {
        public List<Characteristic> characteristic { get; set; }
        public List<ClassOfCategory> classes { get; set; }
        public List<Model_vehicle> models { get; set; }
        public List<Manufacturer> manufacturer { get; set; }
        public List<Package> package { get; set; }
        public List<Vehicle> vehicle { get; set; }
        public List<Category> category { get; set; }

    }
}