﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Register
    {
        [Required(ErrorMessage = "Firstname required.", AllowEmptyStrings = false)]
        public string Firstname { get; set; }
        [Required(ErrorMessage = "Lastname required.", AllowEmptyStrings = false)]
        public string Lastname { get; set; }
        [Required(ErrorMessage = "Email required.", AllowEmptyStrings = false)]
        public string Email { get; set; }
        [Required(ErrorMessage = "Address required.", AllowEmptyStrings = false)]
        public string Address { get; set; }
        [Required(ErrorMessage = "Username required.", AllowEmptyStrings = false)]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password required.", AllowEmptyStrings = false)]
            [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        public string Password { get; set; }        
    }
}